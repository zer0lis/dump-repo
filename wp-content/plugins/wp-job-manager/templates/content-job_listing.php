
<?php global $post; ?>
<li <?php job_listing_class(); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
	<a href="<?php the_job_permalink(); ?>">
		<div class="position col-md-4" style="text-align: left;">
			<span style="color: #000000;"><?php the_title(); ?></span><br />
            <?php the_job_type(); ?>
		</div>
		<div class="location col-md-4" style="text-align: left;">
            <span style="color: #000000;"><?php echo esc_attr( $post->geolocation_city ); ?> <br /></span>
            <?php echo esc_attr( $post->geolocation_country_long ); ?>
			
		</div>
		<ul class="meta col-md-4">
			<?php do_action( 'job_listing_meta_start' ); ?>
			<li class="date"><date><?php printf( __( '%s', 'wp-job-manager' ), date('j/m/Y', strtotime(human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ))); ?></date></li>

			<?php do_action( 'job_listing_meta_end' ); ?>
		</ul>
	</a>
</li>