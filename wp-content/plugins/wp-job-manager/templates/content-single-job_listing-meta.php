<?php
/**
 * Single view Job meta box
 *
 * Hooked into single_job_listing_start priority 20
 *
 * @since  1.14.0
 */
global $post;

do_action( 'single_job_listing_meta_before' ); ?>

<ul class="job-listing-meta meta">
	<?php do_action( 'single_job_listing_meta_start' ); ?>

	<li style="background-color: white;width: 100%;" class="job-type <?php echo get_the_job_type() ? sanitize_title( get_the_job_type()->slug ) : ''; ?>" itemprop="employmentType">
		<div id="single-job-title"><?php echo  the_title(); ?></div><br />
		<a href="mailto:test@cbs.ro" id="job-apply1" style="float:right;">
	        <img src="<?php echo get_bloginfo('template_directory');?>/img/apply.png" class="img-responsive">
	    </a>
		<div id="single-job-category"><?php echo sanitize_title( get_the_job_type()->slug );?>
		</div><br />
		<div id="single-job-date">
		<?php printf( __( '%s', 'wp-job-manager' ), date('j/m/Y', strtotime(human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ))); ?>
		</div><br />
		<div id="single-job-location">
			<?php 	echo esc_attr( $post->geolocation_city ) . ', ';
					echo esc_attr( $post->geolocation_country_short ); ?>
		</div><br />

	</li>
	<hr />

	<!-- <li class="location" itemprop="jobLocation"><?php the_job_location(); ?></li> -->

	<!-- <li class="date-posted" itemprop="datePosted"><date><?php printf( __( 'Posted %s ago', 'wp-job-manager' ), human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ); ?></date></li> -->

	<?php if ( is_position_filled() ) : ?>
		<li class="position-filled"><?php _e( 'This position has been filled', 'wp-job-manager' ); ?></li>
	<?php elseif ( ! candidates_can_apply() && 'preview' !== $post->post_status ) : ?>
		<li class="listing-expired"><?php _e( 'Applications have closed', 'wp-job-manager' ); ?></li>
	<?php endif; ?>

	<?php do_action( 'single_job_listing_meta_end' ); ?>
</ul>

<?php do_action( 'single_job_listing_meta_after' ); ?>
