<?php
/**
 * The services page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for Services Page-->
                <div class="section1-services col-md-12">
                    <h1>Creating headline on 3 rows</h1><br />
                    <h1>for longer concepts</h1><br />
                    <h1>Text random random random</h1>
                </div>

                 <div class="section2-services col-md-12">

                    <div class="mininavbar col-md-6 col-md-offset-3">
                        
                            
                        <div class="group a1 service-active">
                            <span class="bar-left"></span>

                            <img src="<?php echo get_bloginfo('template_directory');?>/img/iconConnected.png" height="40px;" width="60px;" alt="OUTSOURCING" class="img-responsive img">
                            <p>OUTSOURCING</p>
                            <span class="bar-right"></span>
                        </div>

                         

                        <div class="group a2 ">
                           
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/softwaredev.png" alt="SOFTWARE DELIVERY, DEVELOPMENT AND INTEGRATION" class="img-responsive img" height="60px;" width="80px;">
                            <p>SOFTWARE<br /> DEVELOPMENT, DELIVERY<br /> AND INTEGRATION</p>

                            <span class="bar-right"></span>
                        </div>

                        <div class="group a3">

                            <img src="<?php echo get_bloginfo('template_directory');?>/img/cloud.png" height="60px;" width="80px;" alt="CLOUD" class="img-responsive img">
                            <p>CLOUD</p>
                            <span class="bar-right"></span>
                        </div>
                        
                    </div>

                    </div>
                        <div class="mininavbar-second col-md-12">

                                <div id="itoutsourcing" class="fakecheckbox mininavbar-item">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="IT Outsourcing" id="radiocheck" width="22px" height="22px">
                                    <p>it outsourcing</p>
                                </div>

                                <div id="staffaugmentation" class="mininavbar-item">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="IT Outsourcing" id="radiouncheck" width="22px" height="22px">
                                    <p>staff augmentation</p>
                                </div>

                        </div>      
                
                <div id="ajax-wrapper"> 
                     <div class="section3-services col-md-12">
                        <h2>Anytime, Anywhere Access</h2>
                        <p class="col-md-6 col-md-offset-3">You forgot a file on your home computer, or you want to show friends a photo(possibly a cat one) on the train?<br /> No problem. Carbonite mobile apps allow you to access and share your files from your mobile devices,<br /> wherever and whenever you need them.
                        </p>
                        <div class="col-md-12 itoutsourcing-img-wrapper">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
                        </div>
                    </div><!--End of section3-->

                    <div class="section4-services-cloudbackup col-md-12">
                    <h2 class="col-md-12">So, how does this work?</h2>
                        <div class="col-md-3 itoutsourcing-group">

                            <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
                            <h4>Install</h3><br /><br />
                            <p>Install on your computer.</p>
                            
                        </div>

                        <div class="col-md-3 itoutsourcing-group">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
                            <h4>Back up</h4><br /><br />
                            <p>Your files will be backed up<br /> automatically to the cloud.</p>
                        </div>

                        <div class="col-md-3 itoutsourcing-group">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
                            <h4>Restore</h4><br /><br />
                            <p>With your files safely stored in the cloud,<br /> you can get them back anytime.</p>
                        </div>

                        <div class="col-md-3 itoutsourcing-group">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
                            <h4>Access</h4><br /><br />
                            <p>And, you can access them from<br />any internet-connected<br />computer or mobile device.</p>
                        </div>
                        <div class="col-md-12 services-cloudbackup-askforoffer">
                            <a href="#" class="col-md-3 col-md-offset-5"><img src="<?php echo get_bloginfo('template_directory');?>/img/askforoffer.png" alt="Ask for offer" class="img-responsive">
                            </a>
                        </div>
                    </div><!--End of section4-->

                    <div class="section5-services-cloudbackup col-md-12">

                        <div class="col-md-6 cloudbackup-img-wrapper">
                            <div class="col-md-6 cloudbackup-img">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
                            </div>
                            <div class="col-md-6">
                                <h3>Safe and Sound</h3>
                                <p>Fort Know?Even better.Before leaving your computer, your files are encrypted using 128-bit Blowfish encryption.Then, they're transmitted to one of our data centers using Secure Socket Layer(SSL), where they are guarded 24 hours a day.<br/ >
                                Yes, that's the whole, any day.</p>
                            </div>
                        </div>

                         <div class="col-md-6 cloudbackup-img-wrapper">
                            <div class="col-md-6 cloudbackup-img">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
                            </div>
                            <div class="col-md-6">
                                <h3>Easy Restore</h3>
                                <p>If disaster (or spilled coffee) striked, you can recover files from the cloud in just a few clicks. Restore all of your files, or just a few, with our easy restore wizard.</p>
                            </div>
                        </div>

                    </div><!--End of section5-->


                </div><!--End of Ajax Wrapper-->
                <span class="section5-services container-fluid"></span>


             <script type="text/javascript">
                jQuery(document).ready(function(){ //DOCUMENT ACTIVATED, THE DOM IS OURS FOR THE TAKING MUHAHAH

                //Check to see we landed on this page by the redirect from each service page
                  var urlPart = window.location.href.split('?');
                  checkUrl(urlPart[1]);



                //Initial Listeners with callbacks and events
                jQuery('#itoutsourcing').on("click", itoutsourcing);
                jQuery('#staffaugmentation').on("click", staffaugmentation);

                jQuery('.a1').click(a1click);
                jQuery('.a2').click(a2click);  
                jQuery('.a3').click(a3click);

                /*Functions definitions*/

                function checkUrl(string) {
                        if(window.location.href.indexOf(string) > -1 ) {

                           switch(string) {
                                case 'itoutsourcing':
                                    itoutsourcing();
                                    break;
                                case 'staffaugmentation':
                                    staffaugmentation();
                                    break;
                                case 'web-dev':
                                    softwareNavbar();
                                    webapps();
                                    break;
                                case 'custom-development':
                                    softwareNavbar();
                                    customdev();
                                    break; 
                                case 'software-integration-delivery': 
                                    softwareNavbar();
                                    appdelivery();
                                    break;
                                case 'cloud-email':
                                   cloudNavbar();
                                    cloudemail();
                                    break;
                                case 'exchange-email':
                                    cloudNavbar();
                                    exchangeemail();
                                    break;
                                case 'cloud-backup':  
                                    cloudNavbar();
                                    cloudbackup(); 
                                    break;
                                default:
                                    itoutsourcing();
                            } 
                        }
                     }

                //Create a history state of our document for browser navigation(ajax breaks browser navigation and does not update the URI)
                function pushState(url) {
                    window.history.pushState({path: url},'',url); 
                    currentUrl = window.location;  

                    jQuery(window).bind('popstate', function() {
                         jQuery.ajax({
                            url: currentUrl,
                            success: function( data ) {
                                if(window.location.href.indexOf('services') > -1) {//If the previous page was 'services page' append the content in the body
                                    jQuery("body").html(data);
                                } else {
                                    jQuery("#ajax-wrapper").empty().append(data);
                                }
                            }
                        });
                    });
                }

                function a1click() {
                     var outsourcing = '<div id="itoutsourcing" class="fakecheckbox mininavbar-item">\n' +
                                            '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="CBS check" id="radiocheck" width="22px" height="22px">\n' +
                                            '<p>IT Outsourcing</p>\n' +
                                        '</div>\n' +

                                        '<div id="staffaugmentation" class="mininavbar-item">\n' +
                                            '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS uncheck" id="radiouncheck" width="22px" height="22px">\n' +
                                            '<p>Staff Augmentation</p>\n' +
                                        '</div>\n' +
                                    '</div>'

                    jQuery('.mininavbar').find('.service-active').removeClass('service-active');
                    jQuery('.a1').addClass('service-active');
                    jQuery('.a2').find('.bar-right').removeClass('hidden');
                    jQuery('.mininavbar-second').empty().append(outsourcing);

                    jQuery('#itoutsourcing').on("click", itoutsourcing);
                    jQuery('#staffaugmentation').on("click", staffaugmentation);

                    url = "<?php echo site_url( 'itoutsourcing')?>";
                    jQuery.ajax({
                        url: url,
                        success: function( data ) {
                            jQuery('#ajax-wrapper').empty().append(data);
                            window.history.pushState({path: url},'',url); 
                        }
                    });
                }

                function a2click(){
                    softwareNavbar();
                    url = "<?php echo site_url( 'web-dev')?>";
                    jQuery.ajax({
                        url: url,
                        success: function( data ) {

                            jQuery('#ajax-wrapper').empty().append(data);
                            window.history.pushState({path: url},'',url); 
                        }
                    });
                }

                function a3click() {
                    cloudNavbar();
                    url = "<?php echo site_url( 'cloud-email')?>"
                    jQuery.ajax({
                            url: url,
                            success: function( data ) {
                                jQuery('#ajax-wrapper').empty().append(data);
                                window.history.pushState({path: url},'',url); 
                            }
                        });
                }
                //Generate the mininavbar for the Software category
                function softwareNavbar() {
                    var softwaredev = '<div id="webapps" class="fakecheckbox mininavbar-item">\n' +
                                    '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="CBS check" id="radiocheck" width="22px" height="22px">\n' +
                                    '<p>WEB APPLICATIONS AND DESIGN</p>\n' +
                                '</div>\n' +
                                '<div id="customdev" class="mininavbar-item">\n' +
                                    '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS uncheck" id="radiouncheck" width="22px" height="22px">\n' +
                                    '<p>CUSTOM DEVELOPMENT</p>\n' +
                                '</div>\n' +
                                '<div id="appdelivery" class="mininavbar-item">\n' +
                                    '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS uncheck" id="radiouncheck" width="22px" height="22px">\n' +
                                    '<p>APPLICATION DELIVERY AND INTEGRATION</p>\n' +
                                '</div>';

                    jQuery('.mininavbar').find('.service-active').removeClass('service-active');
                    jQuery('.a2').addClass('service-active');
                    jQuery('.a1').find('.bar-right').addClass('hidden');

                    jQuery('.mininavbar-second').empty().append(softwaredev);

                    jQuery('#webapps').on("click", webapps);
                    jQuery('#customdev').on("click", customdev);
                    jQuery('#appdelivery').on("click", appdelivery);
                }
                //Generate the mininavbar for the Cloud category
                function cloudNavbar() {
                    var cloud = '<div id="cloudemail" class="fakecheckbox mininavbar-item">\n' +
                                    '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="CBS check" id="radiocheck" width="22px" height="22px">\n' +
                                    '<p>cloud email</p>\n' +
                                '</div>\n' +
                                '<div id="exchangeemail" class="mininavbar-item">\n' +
                                    '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS uncheck" id="radiouncheck" width="22px" height="22px">\n' +
                                    '<p>exchange email</p>\n' +
                                '</div>\n' +
                                '<div id="cloudbackup" class="mininavbar-item">\n' +
                                    '<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS uncheck" id="radiouncheck" width="22px" height="22px">\n' +
                                    '<p>cloud backup</p>\n' +
                                '</div>';

                    jQuery('.mininavbar').find('.service-active').removeClass('service-active');
                    jQuery('.a3').addClass('service-active');
                    jQuery('.a2').find('.bar-right').addClass('hidden');
                    jQuery('.a1').find('.bar-right').removeClass('hidden');


                    jQuery('.mininavbar-second').empty().append(cloud);

                    jQuery('#cloudemail').on("click", cloudemail);
                    jQuery('#exchangeemail').on("click", exchangeemail);
                    jQuery('#cloudbackup').on("click", cloudbackup);
                }

                function itoutsourcing() {  
                    jQuery('#radiocheck').remove();
                    jQuery('#radiouncheck').remove();

                    jQuery('#staffaugmentation').removeClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#itoutsourcing').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="CBS Service check" id="radiocheck" width="22px" height="22px">');

                    // Make the ajax call, change the URL and register the previous document state for browser navigation
                    url = "<?php echo site_url( 'itoutsourcing')?>";

                     jQuery.ajax({
                        url: url,
                        success: function( data ) {
                            jQuery('#ajax-wrapper').empty().append(data);
                        }
                    });

                     pushState(url);
                    
                }

                 function staffaugmentation() {

                    jQuery('#radiocheck').remove();
                    jQuery('#radiouncheck').remove();
                
                    jQuery('#itoutsourcing').removeClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#staffaugmentation').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="CBS Service check" id="radiocheck" width="22px" height="22px">');

                    url = "<?php echo site_url( 'staff-augmentation')?>";

                    jQuery.ajax({
                        url: url,
                        success: function( data ) {
                            jQuery('#ajax-wrapper').empty().append(data);
                        }
                    });

                    pushState(url);
                }

                function webapps() {
                    jQuery('#radiocheck').remove();
                    jQuery('#radiouncheck').remove(); //Running twice because we have 3 subcategories(2 unchecked radio buttons)
                    jQuery('#radiouncheck').remove();

                    
                    if(jQuery('#customdev').hasClass('fakecheckbox')) { 
                         jQuery('#customdev').removeClass('fakecheckbox');
                     } else {
                        jQuery('#appdelivery').removeClass('fakecheckbox');
                    }
                    jQuery('#customdev').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#appdelivery').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');

                    jQuery('#webapps').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="IT Outsourcing" id="radiocheck" width="22px" height="22px">');

                    url = "<?php echo site_url( 'web-dev')?>";

                    jQuery.ajax({
                        url: url,
                        success: function( data ) {
                            jQuery("#ajax-wrapper").empty().append(data);
                        }
                    });
        
                    pushState(url);
                }

                function customdev() {
                    jQuery('#radiocheck').remove();
                    jQuery('#radiouncheck').remove();
                    jQuery('#radiouncheck').remove();


                    if(jQuery('#webapps').hasClass('fakecheckbox')) {
                        jQuery('#webapps').removeClass('fakecheckbox');
                    } else {
                        jQuery('#appdelivery').removeClass('fakecheckbox');
                    }

                    jQuery('#webapps').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#appdelivery').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#customdev').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="CBS check" id="radiocheck" width="22px" height="22px">');

                    url = "<?php echo site_url( 'custom-development')?>";

                    jQuery.ajax({
                        url: url,
                        success: function( data ) {
                            jQuery('#ajax-wrapper').empty().append(data);
                        }
                    });

                    pushState(url);
                   
                }

                function appdelivery() {
                    jQuery('#radiocheck').remove();
                    jQuery('#radiouncheck').remove();
                    jQuery('#radiouncheck').remove();

                    jQuery('#customdev').hasClass('fakecheckbox') ? jQuery('#customdev').removeClass('fakecheckbox') : jQuery('#webapps').removeClass('fakecheckbox');

                    jQuery('#webapps').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#customdev').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                    jQuery('#appdelivery').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="IT Outsourcing" id="radiocheck" width="22px" height="22px">');

                     url = "<?php echo site_url( 'software-integration-delivery')?>";

                    jQuery.ajax({
                        url: url,
                        success: function( data ) {
                            jQuery('#ajax-wrapper').empty().append(data);
                        }
                    });

                    pushState(url);
                }

                 function cloudemail() {
                        jQuery('#radiocheck').remove();
                        jQuery('#radiouncheck').remove();
                        jQuery('#radiouncheck').remove();

                        jQuery('#exchangeemail').hasClass('fakecheckbox') ? jQuery('#exchangeemail').removeClass('fakecheckbox') : jQuery('#cloudbackup').removeClass('fakecheckbox');

                        jQuery('#exchangeemail').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                        jQuery('#cloudbackup').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                        jQuery('#cloudemail').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="IT Outsourcing" id="radiocheck" width="22px" height="22px">');

                        url = "<?php echo site_url( 'cloud-email')?>";


                        jQuery.ajax({
                            url: url,
                            success: function( data ) {
                                jQuery('#ajax-wrapper').empty().append(data);
                            }
                        });

                        pushState(url);
                    }

                    function exchangeemail() {
                        jQuery('#radiocheck').remove();
                        jQuery('#radiouncheck').remove();
                        jQuery('#radiouncheck').remove();

                        jQuery('#cloudemail').hasClass('fakecheckbox') ? jQuery('#cloudemail').removeClass('fakecheckbox') : jQuery('#cloudbackup').removeClass('fakecheckbox');

                        jQuery('#cloudemail').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                        jQuery('#cloudbackup').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                        jQuery('#exchangeemail').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="IT Outsourcing" id="radiocheck" width="22px" height="22px">');

                        url = "<?php echo site_url( 'exchange-email')?>";

                        jQuery.ajax({
                            url: url,
                            success: function( data ) {
                                jQuery('#ajax-wrapper').empty().append(data);
                            }
                        });

                        pushState(url);
                    }

                    function cloudbackup() {
                        jQuery('#radiocheck').remove();
                        jQuery('#radiouncheck').remove();
                        jQuery('#radiouncheck').remove();

                        jQuery('#cloudemail').hasClass('fakecheckbox') ? jQuery('#cloudemail').removeClass('fakecheckbox') : jQuery('#exchangeemail').removeClass('fakecheckbox');

                        jQuery('#cloudemail').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                        jQuery('#exchangeemail').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiouncheck.png" alt="CBS Service uncheck" id="radiouncheck" width="22px" height="22px">');
                        jQuery('#cloudbackup').addClass('fakecheckbox').prepend('<img src="<?php echo get_bloginfo('template_directory');?>/img/radiocheck.png" alt="IT Outsourcing" id="radiocheck" width="22px" height="22px">');

                        url = "<?php echo site_url( 'cloud-backup')?>";

                         jQuery.ajax({
                            url: url,
                                success: function( data ) {
                                    jQuery('#ajax-wrapper').empty().append(data);
                            }
                        });

                        pushState(url);
                    }
                });
       
            </script>
   <?php 
get_footer();
