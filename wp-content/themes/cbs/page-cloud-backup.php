<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?cloud-backup');
    die();
} ?>
<div class="section3-services col-md-12">
    <h2>Anytime, Anywhere Access</h2>
    <h6 class="col-md-6 col-md-offset-3">You forgot a file on your home computer, or you want to show friends a photo(possibly a cat one) on the train?<br /> No problem. Carbonite mobile apps allow you to access and share your files from your mobile devices,<br /> wherever and whenever you need them.
    </h6>
    <div class="col-md-12 itoutsourcing-img-wrapper">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section3-->

<div class="section4-services-cloudbackup col-md-12">
<h4 class="col-md-12">So, how does this work?</h4>
    <div class="col-md-3 itoutsourcing-group">

        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Install</h4><br /><br />
        <p>Install on your computer.</p>
        
    </div>

    <div class="col-md-3 itoutsourcing-group">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Back up</h4><br /><br />
        <p>Your files will be backed up<br /> automatically to the cloud.</p>
    </div>

    <div class="col-md-3 itoutsourcing-group">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Restore</h4><br /><br />
        <p>With your files safely stored in the cloud,<br /> you can get them back anytime.</p>
    </div>

    <div class="col-md-3 itoutsourcing-group">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Access</h4><br /><br />
        <p>And, you can access them from<br />any internet-connected<br />computer or mobile device.</p>
    </div>
    <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>
</div><!--End of section4-->

<div class="section5-services-cloudbackup col-md-12">

    <div class="col-md-6 cloudbackup-img-wrapper">
        <div class="col-md-6 cloudbackup-img">
            <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        </div>
        <div class="col-md-6">
            <h4>Safe and Sound</h4>
            <p>Fort Know?Even better.Before leaving your computer, your files are encrypted using 128-bit Blowfish encryption.Then, they're transmitted to one of our data centers using Secure Socket Layer(SSL), where they are guarded 24 hours a day.<br/ >
            Yes, that's the whole, any day.</p>
        </div>
    </div>

     <div class="col-md-6 cloudbackup-img-wrapper">
        <div class="col-md-6 cloudbackup-img">
            <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        </div>
        <div class="col-md-6">
            <h4>Easy Restore</h4>
            <p>If disaster (or spilled coffee) striked, you can recover files from the cloud in just a few clicks. Restore all of your files, or just a few, with our easy restore wizard.</p>
        </div>
    </div>

</div><!--End of section5-->