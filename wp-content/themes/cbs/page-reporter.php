<?php
/**
 * The reporter page file.
 *
 * @package CBS
 */

get_header(); ?>
        
        
<!--Specific for Why CBS Page-->

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">
		<div class="section1-reporter" style="border: dotted;">
			<h1>Blog</h1>
		</div>
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();