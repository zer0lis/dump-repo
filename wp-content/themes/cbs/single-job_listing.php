<?php
/**
 * The template for displaying all single jobs.
 *
 * @package CBS
 */

get_header(); ?>

	<div id="primary" class="content-area-single-job">
		<main id="main" class="container-fluid" role="main">

		
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content-single-job_listing', get_post_format() );

			endwhile; // End of the loop.
			?>
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();