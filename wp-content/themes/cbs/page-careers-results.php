<?php
/**
 * The hosted email page file.
 *
 * @package CBS
 */

get_header(); ?>
    <!--Specific for Careers Result Page-->
    <div class="section1-careers-result">
      <ol class="breadcrumb">
        <li><a href="careers.php">Careers</a></li>
        <li class="active">Job Search</li>
      </ol>
    </div>
    <div class="section2-careers-result">
      <div class="col-md-3 col-md-offset-1 form-area">
        <h1>Job Search</h1>
        <div style="padding-bottom: 10px;"></div>
        <div class="container-fluid form-wrapper">
          <form role="form">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Job Title or Keywords">
            </div>
            <div class="dropdown">
              <div class="well dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" placeholder="All areas of talent">All areas of talent
                <span class="caret" style="float: right;"></span>
              </div>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
              </ul>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="pwd" placeholder="City State, Postal Code or Country">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="pwd" placeholder="Language of Job">
            </div>
            <div class="form-group">
              <p>Date Posted</p>
              <div class="form-inline container-fluid">
                <div class="form-inline dateposted col-md-12" >
                  <div class="col-md-6">
                  <input type="radio" name="radiog_dark" id="today" class="css-checkbox" />
                  <label for="today" class="css-label radGroup2">Today</label>
                  </div>
                  <input type="radio" name="radiog_dark" id="7days" class="css-checkbox" checked="checked"/>
                  <label for="7days" class="css-label radGroup2">7 Days</label>
                  <input type="radio" name="radiog_dark" id="30days" class="css-checkbox" />
                  <label for="30days" class="css-label radGroup2">30 Days</label>
                  <input type="radio" name="radiog_dark" id="alltime" class="css-checkbox" />
                  <label for="alltime" class="css-label radGroup2">All time</label>
                </div>
                <div class="form-inline col-md-12">
                  <p class="form-group" style="padding-right  : 20px;">Today</p>
                  <p class="form-group" style="padding-right: 30px;">7 Days</p>
                  <p class="form-group" style="padding-right: 32px;">30 Days</p>
                  <p class="form-group">Any</p>
                </div>
              </div>
              
            </div>
            <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/search.png" alt="Search" class="img-responsive"></a>
          </form>
        </div>
        
      </div>
      <div class="col-md-7 table-area">

<?php $recent = new WP_Query("page_id=110"); while($recent->have_posts()) : $recent->the_post();?>
              <?php the_content(); ?>
              <?php endwhile; ?>
    </div>
    
   
        <script>
          $(document).ready(function(){
              $('.checkradios').checkradios();
          });

          function submitRowAsForm(idRow) {
            form = document.createElement("form"); // CREATE A NEW FORM TO DUMP THE JOB ID INTO FOR SUBMISSION
            form.method = "post"; // CHOOSE FORM SUBMISSION METHOD
            form.action = "careers-details.php"; // TELL THE FORM WHAT PAGE TO SUBMIT TO
            
            input = document.createElement("input"); // CREATE AN ELEMENT TO COPY VALUES TO
            input.type = "hidden";
            input.name = "idjob"; 
            input.value = idRow; // ASSIGN THE VALUE 
            form.appendChild(input);
                 

            form.submit(); // NOW SUBMIT THE FORM THAT WAS JUST CREATED
          };

          $('table.paginated').each(function() { //PAGINATION SCRIPT, IT WAS HARD TO WRITE SO IT SHOULD BE HARD TO READ
              var currentPage = 0;
              var numPerPage = 5;
              var $table = $(this);
              $table.bind('repaginate', function() {
                  $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
              });
              $table.trigger('repaginate');
              var numRows = $table.find('tbody tr').length;
              var numPages = Math.ceil(numRows / numPerPage);
              var $pager = $('<ul class="pagination pagination-lg"><ul>');
              for (var page = 0; page < numPages; page++) {
                  $('<li class="page-number"></li>').text(page + 1).bind('click', {
                      newPage: page
                  }, function(event) {
                      currentPage = event.data['newPage'];
                      $table.trigger('repaginate');
                      $(this).addClass('active').siblings().removeClass('active');
                  }).appendTo($pager).addClass('clickable');
              }
              $pager.insertBefore($table).find('li.page-number:first').addClass('active');
          });

      </script>
  <script src="js/jquery-checkradios/js/jquery.checkradios.min.js"></script>
<?php
get_footer();
