<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CBS
 */

get_header(); ?>
	<?php 
	if(is_page('csr')) {
		get_template_part( 'template-parts/csr');
	}	
	?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main<?php echo '-' . $wp_query->post->post_title; ?>" role="main">
			<!-- <hr />
			<ol class="breadcrumb">
	            <li><a href="careers.php">Careers</a></li>
	            <li class="active">Job Search</li>
          	</ol>
          	<hr /> -->


			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php 
	if(is_page('csr')) {
		echo '<div class="container-fluid csr-space"></div>';
	}	
	?>

<?php
get_footer();
