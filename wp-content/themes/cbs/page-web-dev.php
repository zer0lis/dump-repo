<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?web-dev');
    die();
} ?>
<div class="section3-services col-md-12">
    <h2>Website development from 0 to Cloud</h2>
    <div class="col-md-12 itoutsourcing-img-wrapper">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section3-->

 <div class="section4-services-webdev col-md-12">

    <div class="col-md-6 webdev-group">
        <h4 class="col-md-12">Web app development and integration</h4><br />
        <p class="col-md-12">Enriching your website with creative, out-of-the-box and custom social networking apps, payment solutions, advanced analytics and other tools to increase user engagement</p>
        <div class="col-md-12 webdev-img">        
            <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
        </div>
    </div>

    <div class="col-md-6 webdev-group">
        <h4 class="col-md-12">Mobile friendly</h4><br />
        <p class="col-md-12">Adapting your website for mobile phones and tablets of all platforms and screen sizes as well as using the portal as a backend for a mobile app</p>
        <div class="col-md-12 webdev-img">
            <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
        </div>
    </div>
    
    <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>

</div><!--End of section4-->

<div class="section5-services-webdev col-md-12">
    <div class="col-md-3 col-md-offset-2">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Website development</h4>
        <li>Creating a website from scratch</li><br />
        <li>Client requirements gathering</li><br />
        <li>Design</li><br />
        <li>Implementation</li><br />
        <li>Quality assurance</li><br />
        <li>Maintenance and support</li><br />
    </div>
    <div class="col-md-3">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Redesign</h4>
        <li>Porting your legacy website</li><br />
        <li>Migrate all the database</li><br />
        <li>Redesign a modern slick solution</li><br />
        <li>Design a responsive user interface</li><br />
    </div>
    <div class="col-md-3">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Migration to cloud</h4>
        <li>Moving your existing website and applications to cloud services</li><br />
        <li>Improve scalability</li><br />
        <li>Improve administration</li><br />
        <li>Lessen costs</li><br />
    </div>
</div><!--End of section5 WEB DEV-->

<div class="section6-services-staffaugmentation col-md-12">
    <div class="col-md-2 staffaugmentation-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
    
    <div class="col-md-6 col-md-offset-1">
        <h4>Maintenance and support</h4><br />
        <h6>Our team of experts bring in reliability and stability to any given project, regardless of its complexity or size.Here's why we are the best possible choice:
        </h6><br /><br />
        <li>Creating <b>new features</b> and <b>fixing bugs</b></li><br />
        <li>Enhancing <b>scalibity</b> and <b>performance</b> to welcome the growing number of visitors and data</li><br />
        <li>Improving the website structure to better address any type of <b>user demand</b></li><br />
        <li>Increaing compliance with <b>SEO standards</b> for advanced content marketing strategies</li><br />
    </div>

    <div class="col-md-2 col-md-offset-1  staffaugmentation-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
     <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>
</div><!--End of section6 Staff augmentation-->