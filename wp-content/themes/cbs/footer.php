<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CBS
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">

			<div class="footer-first">
                <div class="col-xs-12">
                    <div class="social-media col-xs-6">
                         <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/iconfacebook.png" class="img-responsive icon" alt="Facebook Icon" height="20"></a>
                         <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/icontwitter.png" class="img-responsive icon" alt="Twitter Icon" height="20"></a>
                         <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/iconlinkedin.png" class="img-responsive icon" alt="LinkedIn Icon" height="20"></a>
                    </div>
                    <div class="newsletter col-xs-6">
                        <label for="text" class="newsletter-label">Newsletter</label>       
                        <input  class="newsletter-input" type="text" placeholder="Email"/>
                    </div>
                </div>

            </div>

            <div class="footer-second col-md-12">
                    <p class="copywrite col-xs-6">All rights reserved. © Cloud Business Services</p>
                    <a class="terms col-xs-6" href="#"><p>Code of conduct</p></a>               
            </div>  

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
