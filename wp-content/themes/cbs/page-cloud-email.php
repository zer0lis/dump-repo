<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?cloud-email');
    die();
} ?>
<div class="section3-services col-md-12">
    <h2>What is CBS Cloud Email all about?</h2>
    <div class="col-md-8 col-md-offset-2 cloud-email">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Hosted business Email</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Calendar</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Contacts</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Accesible from any browser or device</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>100 % Updtime</p>
    </div>
    <div class="col-md-12 itoutsourcing-img-wrapper">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section3-->

 <div class="section4-services-cloudemail col-md-12">
    <div class="col-md-3 col-md-offset-2">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Desktop and laptop</h4>
        <li>Microsoft outlook express</li><br />
        <li>Entourage</li><br />
        <li>Any POP or IMAP desktop software</li><br />
        <li>Mac Mail</li><br />
        <li>Thunderbird</li><br />
    </div>
    <div class="col-md-3">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Web browser</h4>
        <li>Email</li><br />
        <li>Calendar</li><br />
        <li>Tasks</li><br />
        <li>Notes</li><br />
        <li>Contacts</li><br />
    </div>
    <div class="col-md-3">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Mobile devices</h4>
        <li>POP/IMAP</li><br />
        <li>Full mobile sync</li><br />
    </div>
   <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>
</div><!--End of section4-->

<div class="section5-services-cloudemail col-md-12">

    <div class="col-md-6 cloudemail-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div> 

    <div class="col-md-6">
        <h4>Core Features</h4>
        <li><b>Secure</b> POP/IMAP connection</li><br />
        <li><b>25GB</b> mailboxes</li><br />
        <li>Premium <b>Anti-Spam and Anti-Virus</b></li><br />
        <li>Three-layer scanning that keeps your Inbox <b>safe</b>and<b>sound</b></li><br />
        <li><b>Encryption</b> and <b>SSL encryption</b> that hides data during transmission</li><br />
        <li><b>Backup and retrieval</b> - Recover messages in Webmail or recover a deleted mailbox in Control Panel for up to 14 days</li><br />
        <li><b>50MB</b> attachments</li><br />
    </div>

</div><!--End of section5-->