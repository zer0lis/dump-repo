<?php
/**
 * The Play page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for Why CBS Page-->
                <div class="section1-play col-md-12">
                    <h1>Creating headline on 3 rows<br />
                    for longer concepts<br />
                    Text random random random</h1>
                 </div>
                <div class="section2-play col-md-12">
                    <div class="section2-play-text">
                        <h1>HEADLINE HERE</h1>
                        <div class="col-md-8 col-md-offset-2">
                            <p>Today, 95% of the world’s top 1,000 companies have adopted offshore service outsourcing strategies.
                            Outsourcing is by definition, the greatest economic resource for startup companies. It’s basically designed to help them run their business to the fullest, especially in the beginning years, when profit is nowhere to be found and expenses have to be done. </p>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="col-md-12 form-inline section2-play-slider">
                            <span class="space"></span>

                            <div id="play1" class="col-md-3 play section2-play-slider">
                                <a href="" onclick="retu">Play 1</a>
                            </div>
                            <span class="space"></span>
                            <div id="play2" class="col-md-3 play section2-play-slider">
                                <a href="">Play 2</a>
                            </div>
                            <span class="space"></span>
                            <div id="play3" class="col-md-3 play section2-play-slider">
                                <a href="">Play 3</a>
                            </div>
                            <span class="space"></span>
                        </div>
                    </div>
                    <div id="play-wrapper">
                        <div class="col-md-12">
                            <h1>REFER FRIENDS, GET REWARDED</h1>
                            
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat congue finibus.
                                Fusce risus nisi, lacinia vitae turpis a, aliquet vestibulum ex.</p>
                        </div>
                        <div class="section2-play-wrapper col-md-12">
                            <div class="col-md-4">
                                <div class="section2-icon">
                                     <img src="<?php echo get_bloginfo('template_directory');?>/img/cvsend.png" class="img-responsive" alt="Send us your friend CV">
                                </div>
                                <div class="section2-header">
                                <h3>Send us your friends CV</h3>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="section2-icon">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/talk.png" class="img-responsive" alt="Friend Interview" style="margin-left: 100px;">
                                </div>
                                <div class="section2-header">
                                <h3>We will invite your friend to our office for an interview</h3>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="section2-icon">
                                     <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive" alt="Cash Bonus">
                                </div>
                                <div class="section2-header">
                                <h3>Your friends get a job and you get a bonus in cash</h3>
                                </div>
                            </div> 

                        </div> 
                    </div><!--END OF PLAY WRAPPER!-->
                     <div class="askforoffer col-md-12">
                        <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/askforoffer.png" class="img-responsive" alt="Ask for your special offer"></a>   
                    </div> 
                    



                </div>
                <div class="section3-play col-md-12">
                </div>
            <script type="text/javascript">
                jQuery(document).ready(function(jQuery) {

                jQuery('#play1').click(function(e){
                     e.preventDefault();
                    var play1 = '<div id="play-wrapper">\n' +
                                '<div class="col-md-12">\n' +
                                '<h1>REFER FRIENDS, GET REWARDED</h1>\n' +
                                    '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat congue finibus.\n' +
                                    'Fusce risus nisi, lacinia vitae turpis a, aliquet vestibulum ex.</p>\n' +
                            '</div>\n' +
                            '<div class="section2-play-wrapper col-md-12">\n' +
                                '<div class="col-md-4">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cvsend.png" class="img-responsive" alt="Send us your friend CV">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header">\n' +
                                    '<h3>Send us your friend\'s CV</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                                '<div class="col-md-4">\n' +
                                    '<div class="section2-icon">\n' +
                                        '<img src="<?php echo get_bloginfo('template_directory');?>/img/talk.png" class="img-responsive" alt="Friend Interview" style="margin-left: 100px;">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header">\n' +
                                    '<h3>We will invite your friend to our office for an interview</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                                '<div class="col-md-4">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive" alt="Cash Bonus">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header">\n' +
                                    '<h3>Your friends gets a job and you get a bonus in cash</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                            '</div>'; 
                   jQuery('#play-wrapper').replaceWith(play1);
                });

                jQuery('#play2').click(function(e){
                     e.preventDefault();
                     var play2 = '<div id="play-wrapper">\n' +
                                '<div class="col-md-8 col-md-offset-2">\n' +
                                    '<h1>THE ABILITY TO PURSUE RAPID, PROFITABLE GROWTH IS ESSENTIAL TO START-UP BUSINESS</h1>\n' +
                                    '<p>Cloud Business Services understands that, as a start-up, you probably have little or no IT capability, but you need IT systems to operate with low initial costs.While you focus on building your business, you need a strategic partener that takes care of the IT systems required to run the operation and connect with your customer base</p>\n' +
                                    '<h2>We are that strategic partner!</h2>\n' +
                                '</div>\n' +
                                '<div class="section2-play-wrapper col-md-12">\n' +
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cvsend.png" class="img-responsive" alt="Send us your friend CV">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3">\n' +
                                     '<p>Professional email system</p><br />\n' +
                                    '<h3>FREE 12 MONTHS</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                        '<img src="<?php echo get_bloginfo('template_directory');?>/img/talk.png" class="img-responsive" alt="Friend Interview" style="margin-left: 100px;">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3">\n' +
                                     '<p>Website development</p><br />\n' +
                                    '<h3>30% DISCOUNT</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive" alt="Cash Bonus">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3"><br />\n' +
                                     '<p>Professional anti-virus</p>\n' +
                                    '<h3>SPECIAL DISCOUNT</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' + 
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive" alt="Cash Bonus">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3">\n' +
                                    '<p>Maintenance and Support</p><br />\n' +
                                    '<h3>SPECIAL DISCOUNT</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' + 
                            '</div>\n' +
                        '</div>';

                    jQuery('#play-wrapper').replaceWith(play2);
                });

                jQuery('#play3').click(function(e){
                     e.preventDefault();
                    var play3 = '<div id="play-wrapper">\n' +
                                '<div class="col-md-12">\n' +
                                    '<h1>SCHOOLS AND NON-PROFIT ORGANIZATIONS</h1>\n' +
                                    '<p>CBS offers the following benefits:</p>\n' +
                                '</div>\n' +
                                '<div class="section2-play-wrapper col-md-12">\n' +
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cvsend.png" class="img-responsive" alt="Send us your friend CV">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3">\n' +
                                     '<p>Website hosting</p><br />\n' +
                                    '<h3>FREE 12 MONTHS</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                        '<img src="<?php echo get_bloginfo('template_directory');?>/img/talk.png" class="img-responsive" alt="Friend Interview" style="margin-left: 100px;">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3">\n' +
                                     '<p>Website development</p><br />\n' +
                                    '<h3>30% DISCOUNT</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive" alt="Cash Bonus">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3"><br />\n' +
                                     '<p>Professional anti-virus</p>\n' +
                                    '<h3>60% DISCOUNT</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' + 
                                '<div class="col-md-3">\n' +
                                    '<div class="section2-icon">\n' +
                                         '<img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive" alt="Cash Bonus">\n' +
                                    '</div>\n' +
                                    '<div class="section2-header-play3">\n' +
                                    '<p>Maintenance and Support</p><br />\n' +
                                    '<h3>SPECIAL DISCOUNT</h3>\n' +
                                    '</div>\n' +
                                '</div>\n' + 
                            '</div>\n' +
                        '</div>';
                    jQuery('#play-wrapper').replaceWith(play3);
                });                      
            });

            </script>
    <?php
    get_footer();