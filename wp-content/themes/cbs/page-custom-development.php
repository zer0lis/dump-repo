<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?custom-development');
    die();
} ?>
  <div class="section3-services col-md-12">
      <h2>Looking for a dedicated software matched to the<br /> company niche business or processes?</h2>
      <h6>The CBS technology services team has expertise in both front-end and back-end development.<br />Either you need a mobile app, a cloud or a server application, our team's development services is here to develop and deliver the software you are looking for.</h6>
      <div class="col-md-12 itoutsourcing-img-wrapper">
          <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
      </div>
  </div><!--End of section3-->

   <div class="section4-services-staffaugmentation col-md-12">
      <div class="col-md-6 staffaugmentation-img">
          <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
      </div>
      <div class="col-md-6">
          <h4 class="staffaugmentation-header">Technology offerings</h4>

          <div class="col-md-6">

              <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                  <p>C++</p>
              </div><br/>
              <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>.NET</p>
              </div><br />
              <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>Java</p>
              </div><br />
              <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>PHP</p>
              </div><br />
               <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>HTML</p>
              </div><br />

          </div>

          <div class="col-md-6">
              <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                  <p>Mobile</p>
              </div><br/>
              <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>Python</p>
              </div><br />
              <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>IBM Rational</p>
              </div><br />
              <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>IBM WebSphere</p>
              </div><br />
              <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                   <p>Databases</p>
              </div><br />
          </div>
      </div>
      
      <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>

  </div><!--End of section4-->