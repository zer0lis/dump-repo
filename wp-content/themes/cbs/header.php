<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CBS
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-header col-md-12">
			<?php $header_image = get_header_image();?>

            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <img src="<?php echo get_bloginfo('template_directory');?>/img/logocbs.png" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="Logo Cloud Business Services" class=" logocbs"/>
            </a>

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
	<script type="text/javascript">

		jQuery(document).ready(function(){
			services = '<div id="services-navbar" class="col-md-12 ">\n'+
						'<a href="<?php echo site_url( 'itoutsourcing')?>">\n'+
							'<div class="col-md-4">\n'+
								'<div class="col-md-3 col-md-offset-3 services-navbar-img">\n'+
									'<img src="<?php echo get_bloginfo('template_directory');?>/img/iconConnected.png" class="img-responsive" alt="IT Outsourcing">\n'+
								'</div>\n'+
								'<div class="col-md-6 services-navbar-text">\n'+
									'<p>Outsourcing</p>\n'+
								'</div>\n'+
							'</div>\n'+
						'</a>\n'+
						'<a href="<?php echo site_url( 'web-dev')?>">\n'+
							'<div class="col-md-4">\n'+
								'<div class="col-md-3 col-md-offset-3 services-navbar-img">\n'+
									'<img src="<?php echo get_bloginfo('template_directory');?>/img/iconConnected.png" class="img-responsive" alt="IT Outsourcing">\n'+
								'</div>\n'+
								'<div class="col-md-6">\n'+
									'<p>Software Development<br /> Delivery and Integration</p>\n'+
								'</div>\n'+
							'</div>\n'+
						'</a>\n'+
						'<a href="<?php echo site_url( 'cloud-email')?>">\n'+
						'<div class="col-md-4">\n'+
							'<div class="col-md-3 col-md-offset-3 services-navbar-img">\n'+
								'<img src="<?php echo get_bloginfo('template_directory');?>/img/iconConnected.png" class="img-responsive" alt="IT Outsourcing">\n'+
							'</div>\n'+
							'<div class="col-md-6">\n'+
								'<p>Cloud</p>\n'+
							'</div>\n'+
						'</div>\n'+
						'</a>\n'+
					'</div>';

			appendOnce = true;
			jQuery('#menu-item-45').hover(function(){
				if(appendOnce) {
					jQuery('#content').fadeIn('slow', function(){ jQuery('#content').prepend(services) });
					appendOnce = false;
				}
				jQuery('#services-navbar').mouseleave(function(){
					jQuery('#services-navbar').fadeOut(300,function(){ jQuery('#services-navbar').remove()});
					appendOnce = true;
				});
			});
			
			});
		
	</script>
