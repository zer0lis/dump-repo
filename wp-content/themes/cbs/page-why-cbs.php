<?php
/**
 * The why cbs page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for Why CBS Page-->

            <div class="section1-whycbs container-fluid">
                <h1>Why CBS?</h1>
            </div> 

            <div class="section2-whycbs container-fluid">
                <div class="section2-text col-md-8 col-md-offset-2">
                    <h2> Cloud Business Services in a snapshot</h2>
                        <h4>Excellent customer references, dedication to excellence, always available to our customers.</h4>
                </div>
                
                <div class="col-md-12">

                    <div class="group col-md-3">     
                        <div class="group-icon">
                            <span  id="icon1">0
                            </span><span>+</span>
                        </div>
                        <div class="group-header">
                             <h6>IT experienced professionals in cross brand technologies</h6>
                        </div> 
                    </div>
                    
                    <div class="group col-md-3">
                        
                        <div class="group-icon">
                            <span  id="icon2">0
                            </span><span>%</span>
                        </div>
                        <div class="group-header">
                             <h6>and more, of our colleagues, have  8+ years of experience</h6>
                        </div>
                        
                    </div>
                    
                    <div class="group col-md-3">
                        
                        <div class="group-icon">
                            <span  id="icon3">0
                            </span><span class="icon-text">years</span>
                        </div>
                        <div class="group-header">
                             <h6>years of experience in delivering cross brand technology services </h6>
                        </div>
                    </div>
                    
                    <div class="group col-md-3">
                        
                        <div class="group-icon">
                            <span class="icon-text">TOP</span>
                            <span  id="icon4">0
                            </span>
                        </div>
                        <div class="group-header">
                             <h6>companies in Europe and US, in collaboration with</h6>
                        </div>
                   </div>
                   
                </div>
            </div><!--End of section 2!-->

            <div class="section3-whycbs container-fluid">
                <div class="col-md-4 col-md-offset-4">
                    <h2>Intellectual property protection</h2>
                </div>
                <div class="col-md-12">
                     <div class="col-md-6 col-md-offset-3">
                        <p>Piracy is a serious problem in many outsourcing hotspots. The main concern for a company/organization planning to outsource or currently outsourcing is related to security issues (including IP theft and fraud.</p><br /> 
                        <p>Cloud Business Services has an active approach in preventing IP theft and fraud to protect our customers and their organizations. Your intellectual property will be safe with us!
                        </p>
                    </div>
                </div>
            </div> <!--End of Section3!-->

            <div class="section4-whycbs container-fluid">
                <h2>Cloud Business Services has a clear security strategy implemented.</h2>
                <ol class="col-md-4 col-md-offset-1">

                    <li ><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>Customer Segregation<p>
                    </li><br/>
                    <li ><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p >We arrange independent information space for each project</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>Anti-Virus Protection</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>We employ daily-updated antivirus software on each server and workstation and email antivirus software on email software.</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>IP Segregation</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>Public IP addresses are only on Internet-enabled server.</p>
                    </li><br />

                </ol>

                <ol class="col-md-4 col-md-offset-2">
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>Intruder Protocol</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>We use firewalls on internet channels.</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>Cloud Business Services rigorously checks all employees hired. All employees sign NDA and take course on IP rights.</p>
                    </li><br />
                    <li><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                    <p>We screen all employees for criminal background, civil action, any discrepancy in resume, experience, education, references and interviews.</p>
                    </li>
                </ol>
             </div> 

             <div class="section45">
             <h2>Incredible benefits!</h2>
             <div class="col-md-12">
                <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/customerservice.png" class="img-responsive check" alt="Customer Service" height="106px;" width="100px;"></div>
                    <div class="text-wrapper customer-service">
                    <h5>Customer service</h5><br />
                    <p>Improve operations and customer service through tailored solutions.</p>
                    </div>
                </div>
                 <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper better-workflow"><img src="<?php echo get_bloginfo('template_directory');?>/img/betterworkflow.png" class="img-responsive check" alt="Better workflow" height="106px;" width="100px;"></div>
                    <div class="text-wrapper">
                        <h5>Better workflow</h5><br />
                        <p>Allow your business to concentrate on its core competencies</p>
                    </div>
                </div>
                 <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/lowercost.png" class="img-responsive check" alt="Lower costs" width="100px;" height="106px;"></div>
                    <div class="text-wrapper lower-cost">
                        <h5>Lower cost</h5><br />
                        <p>Cloud Business Services helps you save your money and develop your company</p>
                    </div>
                </div>
             </div>

              <div class="col-md-12">
                <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/maximizedprofits.png" class="img-responsive check" alt="Maximized Profits" width="100px;" height="106px;"></div>
                    <div class="text-wrapper maximized-profits">
                        <h5>Maximized profits</h5><br />
                        <p>Cut costs, avoid capital expenditures and employee costs such as insurance and benefits</p>
                    </div>
                </div>
                 <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/highresults.png" class="img-responsive check" alt="High Results" width="100px; height="106px;""></div>
                    <div class="text-wrapper high-results">
                        <h5>High results</h5><br />
                        <p>Increase retention and lay a stable foundation for long-term growth</p>
                    </div>
                </div>
                 <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/extremeflexibiliy.png" class="img-responsive check" alt="Extreme Flexibility" width="100px;" height="106px;"></div>
                    <div class="text-wrapper extreme-flexibility">
                        <h5>Extreme flexibility</h5><br />
                        <p>Achieve flexibility to handle changes in business cycles</p>
                    </div>
                </div>
             </div>

              <div class="col-md-12">
                <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/greatservicedelivery.png" class="img-responsive check" alt="Great Service Delivery" width="100px;" height="106px;"></div>
                    <div class="text-wrapper services-delivery">
                        <h5>Great services delivery</h5><br />
                        <p>Gain access to high trained professionals in cross band technologies</p>
                    </div>
                </div>
                 <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/fullavailability.png" class="img-responsive check" alt="Full availability" width="100px;" height="106px;"></div>
                    <div class="text-wrapper full-availability">
                        <h5>Full availability</h5><br />
                        <p>Allow your business to concentrate on its core competencies</p>
                    </div>
                </div>
                 <div class="col-md-4 group-wrapper">
                    <div class="img-wrapper"><img src="<?php echo get_bloginfo('template_directory');?>/img/customerdelivery.png" class="img-responsive check" alt="Customer Satisfaction" width="100px;" height="106px;"></div>
                    <div class="text-wrapper customer-satisfaction">
                        <h5>Customer satisfaction</h5><br />
                        <p>Develop value-added capabilities that improve customer satisfaction</p>
                    </div>
                </div>
             </div>

             </div>

            <hr size="color:black;border:dotted;">
            <div class="section5">
                <div class="section5-text container-fluid">
                    <h2>They trust us</h2>
                    <div class="col-md-6 col-md-offset-3">
                        <p>Outsourcing is by definition, the greatest economic resource for startup companies. 
                        It’s basically designed to help them run their business to the fullest, especially in the beginning years, when profit is nowhere to be found and expenses have to be done.</p>
                    </div>    
                </div>
                
                <div class="section5-logos">
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                               <!-- Indicators -->
                              <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="img-circle center-block active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class="img-circle center-block"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2" class="img-circle center-block"></li>
                              </ol>

                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="form-inline col-md-12">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logoibm.png" alt="IBM Parteners Logo" class="logo1">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logoaccuro.png" alt="Accuro Logo" class="logo2">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logocaritas.png" alt="Caritas Hospital Logo" class="logo3">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logoavira.png" alt="Accuro Parteners Logo" class="logo4">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="form-inline">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logodell.png" alt="Accuro Parteners Logo" class="logo1">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logoshell.png" alt="Accuro Parteners Logo" class="logo2">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logococacola.png" alt="Accuro Parteners Logo" class="logo3">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logoprocreditbank.png" alt="Accuro Parteners Logo" class="logo4">
                                  </div>
                                </div>
                                <div class="item">
                                    <div class="form-inline">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logoconverse.png" alt="IBM Parteners Logo" class="logo1">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logointel.png" alt="IBM Parteners Logo" class="logo2">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logoavira.png" alt="IBM Parteners Logo" class="logo3">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logocaritas.png" alt="IBM Parteners Logo" class="logo4">
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                    </div><!--end of section5-logos!-->   
            </div><!--end of section5-->
            
            <script type="text/javascript">
            jQuery( document ).ready( function( $ ) {
                $("#icon1").animateNumbers(40);
                $("#icon2").animateNumbers(50);
                $("#icon3").animateNumbers(4);
                $("#icon4").animateNumbers(100);
            } );
            
                
            </script>
       <?php
       get_footer();