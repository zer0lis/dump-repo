<?php
/**
 * The careers page file.
 *
 * @package CBS
 */

get_header(); ?>
            <!--Specific for Careers CBS Page-->
            <div class="section1-careers col-md-12">
                <h1>Careers</h1>
            </div>

            <div class="section2-careers col-md-12">
                <h2>Join CBS. Make a difference.</h2>
                <h4>Text random, Text random,Text random,Text random,Text random,Text random,Text random,</h4>
            
                <div class="col-md-12">
                    <div class="col-md-10 col-md-offset-1">
                        <form class="careers-form  form-inline col-md-12" method="GET" action="<?php echo bloginfo('siteurl') . '/careers-result';?>">

                            <h4 class="col-md-3">Job Search</h4>
                            <div class="careers-jobtitle col-md-3 ">
                                <input type="text" class="input-lg form-control" placeholder="Job Title or Keywords" name="search_keywords">
                            </div>
                            <div class="careers-joblocation col-md-3 ">
                                <input type="text" class="input-lg form-control col-md-3" placeholder="Location" name="search_location">
                            </div>
                            <div class="col-md-3 careers-jobsearch">
                                <a href="#" type="submit" onclick="document.forms[0].submit();return false;">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/search.png" class="img-responsive">
                                </a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            


        <div class="careers-bottom col-md-12"> 
            <img src="<?php echo get_bloginfo('template_directory');?>/img/careersbg.png" class="img-responsive">
        </div>
        <div class="careers-padding col-md-12">
        </div>
<?php 
get_footer();