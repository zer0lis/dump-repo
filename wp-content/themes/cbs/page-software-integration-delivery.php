<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?software-integration-delivery');
    die();
} ?>
 <div class="section3-services col-md-12">
        <h2>Lorem ipsum epsilae nunca facit regnum<br>tuummval et foster ptrium fortis</h2>
        <h6 class="col-md-6 col-md-offset-3">Using some of the most losid integration platforms, like IBM WebSphere, our team can seamlessly integrate the processes and applications within an organization, enabling it to communicate in a secure and flawlessly method.</h6>
        <div class="col-md-12 itoutsourcing-img-wrapper">
            <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
        </div>
    </div><!--End of section3-->

     <div class="section4-services-integration col-md-12">
        <h4 class="h4-center">Why do you need our software integration services?</h4>
        <div class="col-md-4 col-md-offset-1">

            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Applications that were written some time ago, become quickly out of date, and are in need to exchange data with applications written as recently as last week!</p>
            </div><br/>
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Business are constantly evolving</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>There are always new parters to communicate with</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>An application that is used internally in a company is now becoming exposed to customers.</p>
            </div>
        </div>

        <div class="col-md-4 col-md-offset-2">
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Different departments within a company need to share programs.</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Your infrastructure uses HTTP or Homegrown messaging for business critical events</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Leverage a single backbone for everything your integration needs</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Allows you to reliably transport business data</p>
            </div>
        </div>
        
        <div class="col-md-12 askoffer-center">
            <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
        </div>

</div><!--End of section4-->