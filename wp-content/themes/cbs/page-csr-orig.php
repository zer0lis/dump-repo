<?php
/**
 * The csr page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for CSR CBS Page-->
        <div class="section1-csr container-fluid">
        	<h1>Cloud Business Services<br />
                understands the principles of life<br />
                and we share them<br />
                within the team
            </h1>
        </div>
        <div class="section2-csr col-md-12">
        	<h1 class="col-md-12">Our corporate social responsibility program focus</h1>
        	<p class="col-md-6 col-md-offset-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed gravida tellus, eget facilisis lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean ultrices placerat sem, euismod rutrum ligula vulputate nec. Nullam sed nisi ultrices mauris rhoncus rhoncus sed eget nibh. Pellentesque vel mauris ex. Nulla efficitur ipsum eu ligula feugiat, eget maximus nisl sagittis.</p>
        </div>

        <div class="section3-csr col-md-12">
        	<div class="img col-md-6">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr1.png" class="img-responsive">
        	</div>
        	<div class="col-md-6">
	        	<h1>First</h1>
	        	<h2>Youth</h2>
	        	<p>CBS's team is highly involved in helping young children to successfully graduate.<br />
                    Our colleagues participate in after school programs to help the young with their homework.
                </p>
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/lineright1.png" class="img-responsive">
        	</div>
        </div>

        <div class="section4-csr">	
        	<div class="col-md-6">
        		<div class="col-md-12">
	        		<h1 class="col-md-3 col-md-offset-3">Second</h1>
	        	</div>
	        	<div class="col-md-12">
	        		<h2 class="col-md-3 col-md-offset-3">Mentorship</h2>
        		</div>
        		<div class="col-md-12">
	        		<p class="col-md-6 col-md-offset-3">text text text text text text text text text text text text text text text text text text text text</p>
        		</div>
        	</div>
        	<div class="img col-md-6">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr2.png" class="img-responsive">
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/linedown1.png" class="img-responsive">
        	</div>
        </div>
        <div class="section5-csr">
	        <div class="img col-md-6">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr3.png" class="img-responsive">
        	</div>
        	<div class="col-md-6">
	        	<h1>Third</h1>
	        	<h2>Fellow programs</h2>
	        	<p>text text text text text text text text text text text text text text text text text text text text</p>
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/lineright2.png" class="img-responsive">
        	</div>
        </div>
        <div class="section6-csr">
        	<div class="col-md-6">
        		<div class="col-md-12">
	        		<h1 class="col-md-3 col-md-offset-3">Fourth</h1>
	        	</div>
	        	<div class="col-md-12">
	        		<h2 class="col-md-3 col-md-offset-3">Sponsorship</h2>
        		</div>
        		<div class="col-md-12">
	        		<p class="col-md-6 col-md-offset-3">text text text text text text text text text text text text text text text text text text text text</p>
        		</div>
        	</div>
        	<div class="img col-md-6">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr4.png" class="img-responsive">
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/linedown2.png" class="img-responsive">
        	</div>
        </div>

        <div class="section8-csr col-md-12">
	        <h1 class="col-md-4 col-md-offset-2" id="newsheader">News</h1>
	        <div id="myCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			        <div class="group col-md-2 col-md-offset-2">
                        <?php
                        $myposts = get_posts();
                        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                            <h1><?php the_title(); ?> </h1>
                        <?php endforeach; 
                        wp_reset_postdata();?>
                        ?>
			        	<!-- <img src="<?php echo get_bloginfo('template_directory');?>/img/csrbottom.png">
			        	<h1>TITLE</h1>
			        	<p>In June 2014, Mr. Claudiu Gabriel Albina was accepted by the management of Fundatia Inocenti to take part of their volunteer programs. </p> -->
			        </div>
			        <div class="group col-md-2 col-md-offset-1">
			        	<img src="<?php echo get_bloginfo('template_directory');?>/img/csrbottom.png">
			        	<h1>TITLE</h1>
			        	<p>In June 2014, Mr. Claudiu Gabriel Albina was accepted by the management of Fundatia Inocenti to take part of their volunteer programs. </p>
			        </div>
			        <div class="group col-md-2 col-md-offset-1">
			        	<img src="<?php echo get_bloginfo('template_directory');?>/img/csrbottom.png">
			        	<h1>TITLE</h1>
			        	<p>In August 2015 Cloud Business Services started to include in his project proposals a 2% donation from the total amount of the project directed to the Foundation Inocenti – RCR.</p>
			        </div>
		        </div>
		        <div class="item">
			        <div class="group col-md-2 col-md-offset-2">
			        	<img src="<?php echo get_bloginfo('template_directory');?>/img/csrbottom.png">
			        	<h1>TITLE</h1>
			        	<p>In June 2014, Mr. Claudiu Gabriel Albina was accepted by the management of Fundatia Inocenti to take part of their volunteer programs. </p>
			        </div>
			        <div class="group col-md-2 col-md-offset-1">
			        	<img src="<?php echo get_bloginfo('template_directory');?>/img/csrbottom.png">
			        	<h1>TITLE</h1>
			        	<p>In June 2014, Mr. Claudiu Gabriel Albina was accepted by the management of Fundatia Inocenti to take part of their volunteer programs. </p>
			        </div>
			        <div class="group col-md-2 col-md-offset-1">
			        	<img src="<?php echo get_bloginfo('template_directory');?>/img/csrbottom.png">
			        	<h1>TITLE</h1>
			        	<p>In August 2015 Cloud Business Services started to include in his project proposals a 2% donation from the total amount of the project directed to the Foundation Inocenti – RCR.</p>
			        </div>
		        </div>
	         <!-- Left and right controls -->
			  <a href="#myCarousel" role="button" data-slide="prev">
			    <img src="<?php echo get_bloginfo('template_directory');?>/img/stanga.png" class="img-responsive" id="stanga">   
			  </a>
			  <a href="#myCarousel" role="button" data-slide="next">
			    <img src="<?php echo get_bloginfo('template_directory');?>/img/dreapta.png" class="img-responsive" id="dreapta">
			  </a>
		  </div>
        </div>
        </div>
    <?php
    get_footer();