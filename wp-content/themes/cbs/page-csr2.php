<?php
/**
 * The csr page file.
 *
 * @package CBS
 */

get_header();
get_template_part('template-parts/csr');
get_footer();