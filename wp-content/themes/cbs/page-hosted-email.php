<?php
/**
 * The hosted email page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for Hosted Exchange Email Page-->
            <div class="container-fluid">
                <div class="section1">
                    <h1>Hosted Email Exchange</h1>
                </div>
                 <div class="section2-hostedmail">
                    <h2 class="col-md-8 col-md-offset-2">CBS Solution makes it easier for your teams to communicate and get things done with a wide choice of industry-leading email and productivity solutions. Each of our offerings is backed by award-winning Fanatical Support and a team of productivity specialists to help you choose, deploy and manage your email solution.</h2>
                </div> 
                <div class="section3-hostedemail">
                    <div class="col-md-12" style="padding-top: 100px;">

                        <a href="#">
                            <div class="col-md-4 col-md-offset-1 group">
                                <div class="col-md-9 col-md-offset-3">
                                    <h1>Office 365</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla hendrerit nisl, eget viverra dolor tempor ac. Vivamus congue dictum augue, eu facilisis tellus tincidunt et. Ut sed lobortis leo, sit amet sollicitudin ante. Integer varius faucibus nisl, quis euismod metus.</p>
                                </div>
                            </div>
                        </a>

                        <div class="col-md-2"></div>

                        <a href="#"><div class="col-md-4 group-active">
                            <div class="col-md-9 col-md-offset-3">
                                <h1>Hosted Exchange</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla hendrerit nisl, eget viverra dolor tempor ac. Vivamus congue dictum augue, eu facilisis tellus tincidunt et. Ut sed lobortis leo, sit amet sollicitudin ante. Integer varius faucibus nisl, quis euismod metus.</p>
                            </div>
                        </div></a>

                    </div>
                    <div class="col-md-12" style="padding-top: 100px;">
                        <a href="#"><div class="col-md-4 col-md-offset-1 group">
                            <div class="col-md-9 col-md-offset-3">
                                <h1>Office 365</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla hendrerit nisl, eget viverra dolor tempor ac. Vivamus congue dictum augue, eu facilisis tellus tincidunt et. Ut sed lobortis leo, sit amet sollicitudin ante. Integer varius faucibus nisl, quis euismod metus.</p>
                            </div>
                        </div></a>
                        <div class="col-md-2"></div>
                        <a href="#"><div class="col-md-4 group">
                            <div class="col-md-9 col-md-offset-3">
                                <h1>Office 365</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla hendrerit nisl, eget viverra dolor tempor ac. Vivamus congue dictum augue, eu facilisis tellus tincidunt et. Ut sed lobortis leo, sit amet sollicitudin ante. Integer varius faucibus nisl, quis euismod metus.</p>
                            </div>
                        </div></a>
                    </div>
                </div> 
                <div class="section4-hostedemail">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <h1>Unmatched Support</h1>
                            <p class="col-md-10 col-md-offset-1">Our award-winning, Fanatical Support is delivered by email and productivity specialists and is just a call, chat, or ticket away.</p>
                        </div>
                        <div class="col-md-4">
                            <h1>More Choices</h1>
                            <p class="col-md-10 col-md-offset-1">We give you more choices for email and business productivity. Our team of hosting specialists can help you find the right solution to meet your unique needs.</p>
                        </div>
                        <div class="col-md-4">
                            <h1>Deep Expertise</h1>
                            <p class="col-md-10 col-md-offset-1">Each of our email solutions is managed by a dedicated team of experts. We’re uniquely positioned to advise you on the best choices for your business.</p>
                        </div>

                    </div>
                </div>
            </div>
   <?php
   get_footer();