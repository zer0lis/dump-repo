<?php
/**
 * The contact page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for Contact CBS Page-->
                <div class="section1-contact col-md-12">
                    <h1>Contact</h1>
                </div>
                <div class="section2-contact col-md-12">
                    <div class="col-md-6 left">
                        <h1>Reach CBS</h1>
                        <p>Thank you for your interest in CBS. If you have questions about our company, or services, need to report violations of our Acceptable Use Policy or would like to view our legal policies and information, please use the appropriate contact information or links below.</p><br />
                        <div class="currentcustomers">
                        <p>CURRENT CUSTOMERS<br />
                        Phone Support: (+40) 222 5488<br />
                        Chat or Email Support</p><br />
                        </div class="newcustomers">
                        <div>
                        <p>NEW CUSTOMERS<br />
                        Ask CBS questions about becoming a CBS customer.</p><br />
                        </div>
                        <div class="general">
                        <p>GENERAL INQUIRIES<br />
                        Get in touch with CBS and our Romanian-based customer support team. <br />
                        Call (+40) 222 5488</p><br />
                        </div>
                        <div class="parter">
                        <p>PARTNER<br />
                        Ask CBS about our Partner Program to make CBS a part of your product offering, or get help with your existing Partner account.</p><br />
                        </div>
                        <div class="sales">
                        <p>Partner Sales<br />
                        Call (+40) 391 4759<br />
                        Contact us</p><br />
                        </div>
                        <div class="support">
                        <p>Partner Support<br />
                        Call (+40) 334 7603<br />
                        Log in to your account to access support.</p><br />
                        </div>
                        
                    </div>
                    <div class="col-md-6 right" >
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive contactbottom" alt="CBS Headquarter">
                            </div>
                            <div class="contact-text col-md-12">
                                <div class="form-group">
                                    <h2>Global Headquarter</h2>
                                    <p>CBS Office<br /><br />
                                    Putul De Piatra Street, nr. 5<br />    
                                    Bucharest<br />
                                    Romania, PO 78218<br />
                                    (+40) 210 10 10<br />
                                    contact@cbs.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     <?php
     get_footer();