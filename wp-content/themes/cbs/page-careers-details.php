<?php
/**
 * The careers-details page file.
 *
 * @package CBS
 */
 
get_header(); ?>      
    <!--Specific for Careers Details Page-->
    <div class="container-fluid">
        <div class="section-career-details col-md-10 col-md-offset-1">
          <ol class="breadcrumb">
            <li><a href="careers.php">Careers</a></li>
            <li class="active">Job Search</li>
          </ol>
          <?php 
          $jobid = $_POST['idjob']; 
          $job = get_job_by_id($jobid)->fetch_assoc(); ?> 
          <div class="section1-career-details col-md-12">
            <div class="col-md-6"> 
              <h1> <?php echo $job['title']; ?> </h1>
              <p> <?php echo $job['category']; ?> </p>
              <p> <?php echo date('j/m/Y', strtotime($job['dateCreated'])); ?> </p>
              <p> <?php echo $job['city'] . ', ' . $job['country']; ?> </p>
            </div>
            <div class="applyright">
              <a href=""><img src="<?php echo get_bloginfo('template_directory');?>/img/apply.png"></a>
            </div>
          </div>
          <div class="description col-md-12">
            <h1>Description</h1>
            <hr>
            <p> <?php echo $job['description']; ?> </p>
            <h4>Job Duties:</h4>
            <p><?php echo $job['duties']; ?></p>
          </div>
          <div class="qualifications col-md-12">
            <h1>Qualifications</h1>
            <hr>
            <h4>Basic Qualifications:</h4>
            <p><?php echo $job['qualificationBasic']; ?></p>
            <h4>Preffered Qualifications:</h4>
            <p><?php echo $job['qualificationPref']; ?></p>
          </div>
          <div class="applybottom col-md-12">
            <a href=""><img src="<?php echo get_bloginfo('template_directory');?>/img/apply.png"></a>
          </div>
        </div>

    </div>


  <?php 
get_footer();