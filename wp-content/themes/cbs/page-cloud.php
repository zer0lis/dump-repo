<?php
/**
 * The cloud page file.
 *
 * @package CBS
 */

get_header(); ?>
            <!--Specific for Cloud CBS Page-->
            <div class="container-fluid">
                <div class="section1-cloud">
                    <h1>Creating headline on 3 rows<br />
                    for longer concepts<br />
                    Text random random random</h1>
                </div>
                <div class="section2-cloud">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue, lorem sit amet suscipit finibus, eros orci viverra dui, eu egestas quam dui convallis elit. Ut id leo odio. Pellentesque mollis elementum purus, vitae ullamcorper eros ultricies quis.</h2>
                    </div>
                    <div class="section2-cloud-main col-md-12">
                        <div class="col-md-4">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/hostedemail.png" type="img-responsive" alt="Hosted Email Exchange" height="120px">
                            <div class="section2-cloud-header">
                                <h2>Hosted Email Exchange</h2>
                            </div>
                            <a href="<?php echo site_url('email')?>"><img class="find-solutions" src="<?php echo get_bloginfo('template_directory');?>/img/ButtonFindSolutions.png" type="img-responsive" alt="Find Solutions"></a>
                        </div> 
                        <div class="col-md-4">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/cloudbackup.png" type="img-responsive" alt="Cloud Backup" height="120px">>
                            <div class="section2-cloud-header">
                                <h2>Cloud Backup</h2>
                            </div>
                            <a href="<?php echo site_url('hosted-email')?>"><img src="<?php echo get_bloginfo('template_directory');?>/img/ButtonFindSolutions.png" type="img-responsive" alt="Find Solutions"></a>
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo get_bloginfo('template_directory');?>/img/webhosting.png" type="img-responsive" alt="Web Hosting" height="120px">>
                            <div class="section2-cloud-header">
                                <h2>Web Hosting</h2>
                            </div>
                            <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/ButtonFindSolutions.png" type="img-responsive" alt="Find Solutions"></a>
                        </div>
                    </div>
                </div>
                <div class="section3-cloud">
                    <h5>People are talking...</h5><br />

                    <h3>"What I like about CBS is that you choose your plan and it just works."</h3><br />

                    <h3>Lorem Ipsun, CBS Customer</h3> 
                    </h3>
                </div>
                <div class="section4-cloud">
                    <div class="form-group">
                        <div class="col-md-12">
                            <h1>Have any questions?</h1>
                        </div>
                        <div class="col-md-12">
                            <h3>We can help.</h3>
                        </div>
                        <div class="col-md-12">
                            <a href="#" class="link"><img src="<?php echo get_bloginfo('template_directory');?>/img/contactus.png" class="img-responsive" alt="Contact Us"></a>
                        </div>
                    </div>
                </div>
            </div>

   
       <?php 
get_footer();