<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CBS
 */

?>

<article class="post col-md-12" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="postimg">
		<?php the_post_thumbnail( array(1100, 800)); ?>
	</div>
	<header class="entry-header container-fluid">
		<div class="entry-meta col-md-2 col-md-offset-1">
			<?php cbs_posted_on(); ?>
		</div><!-- .entry-meta -->
		
		<div class="col-md-8" id="content-wrapper">
			<div class="blog-post-title">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</div>
			<div class="excerpt">
			<?php
				the_excerpt( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'cbs' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cbs' ),
					'after'  => '</div>',
				) );
				
				echo get_the_excerpt();
			?>
			</div>

			<footer class="entry-footer col-md-12">
					<div class="entry-by col-md-12">
						<span class="byline col-md-4"><?php echo 'by ';the_author(); ?></span>
						<span class="sep"></span>
						<span class="cat-links col-md-4">
						<?php 
						$categories = get_the_category();
						$separator = ' ';
						$output = '';
						if ( ! empty( $categories ) ) {
						    foreach( $categories as $category ) {
						        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
						    }
						    echo trim( $output, $separator );
						} ?>
						</span>
						<span class="sep"></span>
						<span class="comments-link col-md-4"><a href="<?php comments_link(); ?>"> Comment this post</a></span>
					</div>
					
			</footer><!-- .entry-footer -->
		</div>
		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_excerpt( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'cbs' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cbs' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	
</article><!-- #post-## -->
