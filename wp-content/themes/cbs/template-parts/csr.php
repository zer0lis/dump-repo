<?php
/**
 * The csr page file.
 *
 * @package CBS
 */
?>
            <!--Specific for CSR CBS Page-->
        <div class="section1-csr col-md-12">
        	<h1>Cloud Business Services<br />
                understands the principles of <br />
                life and we share them<br />
                within the team.</h1>
        </div>

        <div class="section2-csr col-md-12">
        	<h1 class="col-md-12">Our corporate social responsibility program focus</h1>
        	<p class="col-md-6 col-md-offset-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed gravida tellus, eget facilisis lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean ultrices placerat sem, euismod rutrum ligula vulputate nec. Nullam sed nisi ultrices mauris rhoncus rhoncus sed eget nibh. Pellentesque vel mauris ex. Nulla efficitur ipsum eu ligula feugiat, eget maximus nisl sagittis.</p>
        </div>

        <div class="section3-csr col-md-12">
        	<div class="img col-md-6">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr1.png" class="img-responsive" alt="CBS team help">
        	</div>
        	<div class="col-md-6">
	        	<h1>First</h1>
	        	<h2>Youth</h2><br /><br />
	        	<p>CBS's team is highly involved in helping young children to successfully graduate.<br />
                    Our colleagues participate in after school programs to help the young with their homework.
                </p>
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/lineright1.png" class="img-responsive" alt="line">
        	</div>
        </div>

        <div class="section4-csr">	

        	<div class="col-md-6">
        		<div class="col-md-12">
	        		<h1 class="col-md-3 col-md-offset-3">Second</h1><br />
	        		<h2 class="col-md-3 col-md-offset-3">Mentorship</h2>
        		</div>
        		<div class="col-md-12">
	        		<br /><p class="col-md-6 col-md-offset-3">text text text text text text text text text text text text text text text text text text text text</p>
        		</div>
        	</div>
        	<div class="img col-md-4">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr2.png" class="img-responsive" alt="CBS Menthorship">
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/linedown1.png" class="img-responsive" alt="CBS line">
        	</div>

        </div>

        <div class="section5-csr">
	        <div class="img col-md-6">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr3.png" class="img-responsive" alt="CBS Programs">
        	</div>
        	<div class="col-md-6">
	        	<h1>Third</h1>
	        	<h2>Fellow programs</h2><br />
	        	<p>text text text text text text text text text text text text text text text text text text text text</p>
        	</div>
        	<div class="img-center col-md-12">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/lineright2.png" class="img-responsive" alt="CBS line">
        	</div>
        </div>
        <div class="section6-csr">
        	<div class="col-md-6">
        		<div class="col-md-12">
	        		<h1 class="col-md-3 col-md-offset-3">Fourth</h1>
	        		<h2 class="col-md-3 col-md-offset-3">Sponsorship</h2>
        		</div>
        		<div class="col-md-12">
	        		<br /><p class="col-md-6 col-md-offset-3">text text text text text text text text text text text text text text text text text text text text</p>
        		</div>
        	</div>
        	<div class="img col-md-4">
        		<img src="<?php echo get_bloginfo('template_directory');?>/img/csr4.png" class="img-responsive" alt="CBS Sponsorship">
        	</div>
        </div>

        <div class="section8-csr col-md-12">
           <div class="container-fluid csr-space"></div>
	        <h1 class="col-md-4 col-md-offset-1" id="newsheader">News</h1>
        </div>
        