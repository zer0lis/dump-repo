<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?exchange-email');
    die();
} ?>
<div class="section3-services col-md-12" >
    <h2>Your Business Class Emails</h2>
    <div class="col-md-8 col-md-offset-2 cloud-email">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Reliable business class email</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Low and predictable costs</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Wireless mobile sync</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Email antivirus</p>
    </div>
    <div class="col-md-8 col-md-offset-2 cloud-email">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>100 % uptime guarantees</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Add users as your business grow<br />and only pay for what you use</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Migration of your old email to<br /> CBS Cloud email</p>
        <img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
        <p>Automatic upgrades</p>
    </div>
    <div class="col-md-12 itoutsourcing-img-wrapper">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section3-->

<div class="section5-services-exchangeemail col-md-12">

    <div class="col-md-6 exchangeemail-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div> 

    <div class="col-md-6">
        <h4>Core Features</h4>
        <div class="col-md-6">
            <li><b>Exchange Hybrid</b></li><br />
            <li><b>100GB</b> mailboxes</li><br />
            <li>Premium <b>Anti-Spam and Anti-Virus</b></li><br />
            <li>Three-layer scanning that keeps your Inbox <b>safe</b>and<b>sound</b></li><br />
            <li><b>Outlook Web App</b>(OWA)</li><br />
            <li><b>Encryption</b> and <b>SSL encryption</b> that hides data during transmission</li><br />
            <li><b>Backup and retrieval</b> - Recover messages in Webmail or recover a deleted mailbox in Control Panel for up to 14 days!Plus, you restore an entire deleted mailbox for up to 30 days!</li><br />
            <li><b>50MB</b> attachments</li><br />
            <li>Manage your mail from any <b>browser, anywhere, anytime</b></li><br />
            <li>iPhone <b>auto-setup</b></li><br />
        </div>

        <div class="col-md-6">
            <li><b>Quick user connection</b> to iPhone email using only email credentials</li><br />
            <li>Email Archiving</li><br />
            <li>Shared contacts - Available company-wide via Global Address List(GAL)</b></li><br />
            <li>Shared calendar - Share your calendar or view other's calendars, making scheduling a breeze.</li><br />
            <li>Distribution lists- Unlimited distribution lists for sharing information with specific groups</li><br />
            <li>Notes and tasks - The productivity apps you depend on</li><br />
            <li>Send-As permissions -  Allow another person to send and accept email on your behalf - perfect for executive assistants.</li><br />
        </div>

    </div>
     <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>
</div><!--End of section5-->