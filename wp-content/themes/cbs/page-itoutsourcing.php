<?php 
//If accesed directly redirect to the main service page
if(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
    $url = site_url( 'services'); 
    header('Location: '.$url. '?itoutsourcing');
    die();
} ?>

<div class="section3-services col-md-12">
    <h2>Headline just needed some words</h2>
    <h6 class="col-md-6 col-md-offset-3">The CBS technology services team has expertise both is front-end back-end development.
    Either you need a mobile app, a cloud or a server application our CBS development services team is here to develop and deliver the software you are looking for. 
    </h6>
    <div class="col-md-12 itoutsourcing-img-wrapper">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section3-->

 <div class="section4-services col-md-12">

    <div class="col-md-3 itoutsourcing-group">

        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>End User Services</h4>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>IT Service Desk</p>
        </div><br/>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Desktop Technician Support</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Desktop Software Support</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Mobility Support Services</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>IT Asset Management</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Networking Management and Support</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>IT Security</p>
        </div><br />

    </div>

    <div class="col-md-3 itoutsourcing-group">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Application Management</h4>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Software Management</p>
        </div><br/>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Corrective Meintanance</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Preventive Meintanance</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Software Support Services</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Application Testing</p>
        </div><br />
    </div>

    <div class="col-md-3 itoutsourcing-group">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Hardware Management</h4>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Server Administration and Meintenance</p>
        </div><br/>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Storage Management and Meintenance</p>
        </div><br />
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>HW Security Management</p>
        </div><br />
    </div>

    <div class="col-md-3 itoutsourcing-group">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/cash.png" class="img-responsive itoutsourcing-img" alt="Cash Bonus">
        <h4>Networking</h4>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
            <p>Infrastructure Monitoring</p>
        </div><br/>
        <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
             <p>Infrastructure Meintenance</p>
        </div><br />
    </div>
    
    <div class="col-md-12 askoffer-center">
       <a href="" class="askoffer btn"><p>Ask for your special offer</p></a>
    </div>
</div><!--End of section4-->