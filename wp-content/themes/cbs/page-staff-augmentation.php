<?php if(!$_SERVER['HTTP_X_REQUESTED_WITH'])
{
    $url = site_url( 'services'); 
    header('Location: '.$url.'?staffaugmentation');
    die();
} ?>

<div class="section3-services col-md-12">
    <h2>Our talented team is ready to scale up your team</h2>
    <h6 class="col-md-6 col-md-offset-3">Cloud Business Services provides skilled individual developers on demand as well as dedicated development
    teams to augment your in-house team to cover specific technical needs, that you may encounter along the way.
    </h6>
    <div class="col-md-12 itoutsourcing-img-wrapper">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section3-->

 <div class="section4-services-staffaugmentation col-md-12">
    <div class="col-md-6 staffaugmentation-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
    <div class="col-md-6">
        <h4>Technology offerings</h4>

        <div class="col-md-6">

            <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                <p>Major databses, including both IBM and third-party</p>
            </div><br/>
            <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>DevOps</p>
            </div><br />
            <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>IBM Rational</p>
            </div><br />
            <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>Packaged applications from IBM and leading ISVs<br />
                 Mainframe transaction processing</p>
            </div><br />
            <div class="staffaugmentation-check"><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>IBM Middleware(Websphere Application Server, Message Broker, MQ, Porta, Data Power, e-commerce)</p>
            </div><br />

        </div>

        <div class="col-md-6">
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                <p>Major operating system(Windows, Linux, AIX)</p>
            </div><br/>
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>Web technologies</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>Networking</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>Java</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>Core SAP(FI, CO, AM, PS, HR, PM, MM, QM, PP, SD)</p>
            </div><br />
            <div><img src="<?php echo get_bloginfo('template_directory');?>/img/check.png" class="img-responsive check" alt="CBS Check">
                 <p>SAP ABAP, BASIS</p>
            </div><br />
        </div>
    </div>

    <div class="col-md-12 askoffer-center">
        <a href="" class="askoffer">Ask for your special offer</a>
    </div>

</div><!--End of section4-->

<div class="section5-services-staffaugmentation col-md-12">
    <div class="col-md-5 col-md-offset-1">
        <h4>Agile cooperation</h4><br />
        <h6>By hiring Cloud Business Services' dedicated teams and developers on flexible terms, you take full control of the resources and are able to re-alocate them according to your current needs.</h6><br />
        <p>With our staff board, you can also count on:</p>
        <ul>
            <li>Short ramp-up time(2-4 weeks)</li>
            <li>Easily scale up and down - no employee idle time and waiting on the bench,<br /> you hire only the specialists you need and pay only for the time when you need them.</li>
            <li>Quick termination with a 3 month notice period</li>
        </ul>
         <a href="" class="askoffer">Ask for your special offer</a>
    </div>
    <div class="col-md-6 staffaugmentation-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
</div><!--End of section5 Staff augmentation-->

<div class="section6-services-staffaugmentation col-md-12">
    <div class="col-md-2 staffaugmentation-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>
    
    <div class="col-md-6 col-md-offset-1">
        <h4>WHY HIRE Cloud Business Services DEVELOPERS?</h4><br />
        <h6>Our team of experts bring in reliability and stability to any given project, regardless of its complexity or size.Here's why we are the best possible choice:
        </h6><br /><br />
        <li><b>8+ years</b> of professsional experience</li><br />
        <li><b>over 50% of developers</b> are Seniors and Leads</li><br />
        <li> expecienced in <b>large-scale projects</b> with distributed international teams sized 40-50 specialists as well as <b>projects with complex architectures</b></li><br />
        <li><b>technological and industry knowledge</b> that goes beyond the standard programming tools and applies the right framework tailored to the client's requirements</li><br />
        <li><b>various software development methodologies</b> available</li><br />
        <li><b>fluency in English</b></li><br />
        <div class="col-md-12 staffaugmentation-img">
              <a href="" class="askoffer">Ask for your special offer</a>
        </div>
    </div>

    <div class="col-md-2 col-md-offset-1  staffaugmentation-img">
        <img src="<?php echo get_bloginfo('template_directory');?>/img/contactbottom.png" class="img-responsive" alt="CBS Headquarter">
    </div>

</div><!--End of section6 Staff augmentation-->