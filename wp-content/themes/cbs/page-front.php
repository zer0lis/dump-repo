<?php
/**
 * The front page file.
 *
 * @package CBS
 */

get_header(); ?>
        
				<div class="section1 col-md-12">
                    <div class="form-inline">
                        <h1>Focus on your business<br /> We'll take care of the rest.</h1>
                        <a class="calltoaction btn" href="#"><p>Call to action &nbsp &nbsp <span class="glyphicon glyphicon-circle-arrow-right"></span></p></a>
                    </div>			
				</div>
				<div class="section2 col-md-12">
                    <div class="col-md-10 col-md-offset-1">
    					<div class="section2-text col-md-10 col-md-offset-1">
    						<h2> 95% of the world’s top 1,000 companies have adopted offshore service outsourcing strategies</h2>
    						<h6>Outsourcing is by definition, the greatest economic resource for startup companies. 
                                It’s basically designed to help them run their business to the fullest, especially in the beginning years, when profit is nowhere to be found and expenses have to be done.
                            </h6>
    					</div>
                    </div>
                    
					<div class="col-md-12">
                        
						<div class="group col-md-4">     
                            <div class="group-icon">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/iconConnected.png" class="img-responsive" alt="IT Outsourcing">
                            </div>
                            <div class="group-header">
							     <h4>IT Outsourcing</h4>
                            </div>
                            <div class="group-button">						
							    <a href="#"><p>Find Solutions <span class="glyphicon glyphicon-chevron-right"></span></p></a>
                            </div>    
						</div>
                        
						<div class="group col-md-4">
                            
                            <div class="group-icon">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/softwaredev.png" class="img-responsive" alt="Software Development">
                            </div>
                            <div class="group-header">
							     <h4>Software Development and Delivery</h4>
                            </div>
                            <div class="group-button">						
							    <a href="#"><p>Find Solutions <span class="glyphicon glyphicon-chevron-right"></span></p></a>
                            </div>
                            
						</div>
                        
						<div class="group col-md-4">
                            
                            <div class="group-icon">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/cloud.png" class="img-responsive" alt="Cloud">
                            </div>
                            <div class="group-header">
							     <h4>Cloud</h4>
                            </div>
                            <div class="group-button">						
							     <a href="#"><p>Find Solutions <span class="glyphicon glyphicon-chevron-right"></span></p></a>
                            </div>
					   </div>
                       
				    </div>
                </div><!--End of section 2!-->
				<div class="section3 col-md-12">
                    <div class="section3-text col-md-4 col-md-offset-1">
                        <h3>Meet all your goals with no risk</h3>
                        <p>Whether you need IT resources for two months, three years, or need them on a permanent basis</p>
                       <a class="calltoaction btn" href="#"><p>Call to action &nbsp &nbsp <span class="glyphicon glyphicon-circle-arrow-right"></span></p></a>
                    </div>
                    
				</div>
                
				<div class="section4 col-md-12">           

                    <div class="col-md-6 section4-container">
                        <img src="<?php echo get_bloginfo('template_directory');?>/img/racheta.png" class="img-responsive" alt="Second Slider" id="rocket">
                    </div>
                    
                    <div class="section4-text col-md-6">
                        <div class="col-md-9">
                                <h4>Play 2</h4>
                                <h2>Start UP</h2>
                                <p>The ability to pursue rapid, profitable growth is essential to start-up businesses.<br /><br /> Cloud Business Services understand that as a start-up, you probably have little or no IT capability and need IT systems to operate with low initial costs. While you focus on building your business, you need a strategic partner that takes care of the IT systems required to run the operation and connect with your customer base
                                </p>
                                <a class="calltoaction btn" href="#"><p>Call to action &nbsp&nbsp<span class="glyphicon glyphicon-circle-arrow-right"></span></p></a>
                        </div>
                    </div>
                    <div id="stanga">
                        <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/stanga.png" class="img-responsive" alt="Second Slider" width="28" height="48"></a>
                    </div>
                    <div id="dreapta">
                        <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/dreapta.png" class="img-responsive" alt="Second Slider" width="28" height="48"></a>
                    </div>
				</div><!--end of section4!-->
                
				<div class="section5 col-md-12">
                    
                    <div class="section5-text col-md-12">
                        <h2>They trust us</h2>
                        <div class="col-md-6 col-md-offset-3">
                            <p>Outsourcing is by definition, the greatest economic resource for startup companies. 
                            It’s basically designed to help them run their business to the fullest, especially in the beginning years, when profit is nowhere to be found and expenses have to be done.</p>
                        </div>    
                    </div>
                    
                    <div class="section5-logos">
                        <div class="col-md-12">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                               <!-- Indicators -->
                              <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="img-circle center-block active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class="img-circle center-block"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2" class="img-circle center-block"></li>
                              </ol>

                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="col-md-12">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logoibm.png" alt="IBM Parteners Logo" class="logo1">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logoaccuro.png" alt="Accuro Logo" class="logo2">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logocaritas.png" alt="Caritas Hospital Logo" class="logo3">
                                      <img src="<?php echo get_bloginfo('template_directory');?>/img/logoavira.png" alt="Accuro Parteners Logo" class="logo4">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-12">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logodell.png" alt="Accuro Parteners Logo" class="logo1">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logoshell.png" alt="Accuro Parteners Logo" class="logo2">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logococacola.png" alt="Accuro Parteners Logo" class="logo3">
                                  <img src="<?php echo get_bloginfo('template_directory');?>/img/logoprocreditbank.png" alt="Accuro Parteners Logo" class="logo4">
                                  </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-12">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logoconverse.png" alt="IBM Parteners Logo" class="logo1">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logointel.png" alt="IBM Parteners Logo" class="logo2">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logoavira.png" alt="IBM Parteners Logo" class="logo3">
                                    <img src="<?php echo get_bloginfo('template_directory');?>/img/logocaritas.png" alt="IBM Parteners Logo" class="logo4">
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                 
                    
                </div><!--end of section5-logos!-->   
			</div><!--end of section5-->
<?php 
get_footer();
