<?php
/**
 * The email page file.
 *
 * @package CBS
 */

get_header(); ?>
        
            <!--Specific for Email Exchange Page-->
            <div class="container-fluid">
                <div class="section1-email">
                    <h1>Hosted Exchange e-mail</h1>
                </div>
                <div class="section2-email">
                    <h3>Are you still managing your Exchange Server?</h3>
                    <p class="col-md-6 col-md-offset-3">Say yes to Microsoft Exchange and no to the cost and headaches of managing it. Our deep Microsoft hosting knowledge is just one reason we're a leading Exchange provider. Plus, with Hosted Exchange 2016 at Rackspace you get huge 100GB mailboxes and 50MB attachments.</p>
                </div>
                <div class="col-md-12 section2-email-block">
                    <div class="col-md-6">
                        <div class="block col-md-12">
                            <h3>Uptime Guarantee</h3>
                            <p>When your email is down, so is your business. We know how downtime impacts your productivity and your bottom line. That's why we back Hosted Exchange with an unmatched Network Uptime Guarantee and SLA.</p>
                            <div class="col-md-12 col-md-offset-1">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/exchangeemailuptime.png" class="img-responsive" alt="Uptime Guarantee">
                            </div>
                        </div>
                        <div class="block col-md-12">
                            <h3>Save with Exchange Hybrid</h3>
                            <p>Combine Hosted Exchange with CBS Email and save money. Give your power users the robust feature set of Microsoft Exchange, while lighter users can enjoy affordable, business-class Rackspace Email—all on the same domain. Learn more about Exchange Hybrid.</p>
                            <div class="col-md-12 col-md-offset-1">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/exchangeemailmulti.png" class="img-responsive" alt="Hybrid">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="block col-md-12">
                            <h3>Improved Security</h3>
                            <p>Hosted Exchange offers the security your business requires. Our fully managed data centers, along with three Anti-Spam and Anti-Virus scans and SSL encryption for all email transmissions, add more protection for your sensitive mail. Learn more about the features of Hosted Exchange.</p>
                            <div class="col-md-12 col-md-offset-1">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/exchangeemailmoney.png" class="img-responsive" alt="Security">
                            </div>
                        </div>
                        <div class="block col-md-12">
                            <h3>Improved Security</h3>
                            <p>Hosted Exchange offers the security your business requires. Our fully managed data centers, along with three Anti-Spam and Anti-Virus scans and SSL encryption for all email transmissions, add more protection for your sensitive mail. Learn more about the features of Hosted Exchange.</p>
                            <div class="col-md-12 col-md-offset-1">
                                <img src="<?php echo get_bloginfo('template_directory');?>/img/exchangeemailconnected.png" class="img-responsive" alt="Outlook">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-md-offset-5 link" style="padding-bottom: 100px; padding-top: 100px;">
                    <a href="#"><img src="<?php echo get_bloginfo('template_directory');?>/img/askforoffer.png" alt="Ask for your special offer"></a>
                </div>
            </div>
  <?php
  get_footer();